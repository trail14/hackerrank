import textwrap

def wrap(string,max_width):
    wrap=textwrap.TextWrapper(width= max_width)
    word_list=wrap.wrap(text=string)
    for result in word_list:
        print(result)
if __name__ == '__main__':
    string, max_width = input(), int(input())
    result = wrap(string, max_width)
    # print(result)